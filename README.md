
# DisKan - A discord bot for KanBoard via its API

This bot is a work in progress, its goal is to allow you to manage and view the information about your kanboard projects easily and efficently.

Currently, you can view a summary of your currently opened tasks and information about your projects, the bot will auto populate its information from the API at your kanboard installation.


![alt text](https://git.codingvm.codes/snxraven/DisKan/raw/branch/master/menu.png)
![alt text](https://git.codingvm.codes/snxraven/DisKan/raw/branch/master/project%20details.png)


# Installation Steps:

Clone the Repo:
```
git clone https://git.codingvm.codes/snxraven/DisKan.git
```

Change to the DisKan dir:
```
cd DisKan
```

NPM Install:
```
npm install
```

Edit the .env.default config to your needs:
```
DISCORD_TOKEN=
OWNER=
ROOT_DOMAIN=board.yourdomain.com
KANBOARD_API_KEY=
PER_PAGE=2
```

Start the bot:
```
node index.js
```

Pull requests are welcome :) 

