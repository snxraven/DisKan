// Needed for FS operations
const fs = require("fs");

// Init our global config
require("dotenv").config();

// Import our needed HTTP Lib
var http = require('unirest');

// Grab RUN - get Client, Message, Command arguemtns and permission level from the bot
exports.run = (client, message, args, level) => { // eslint-disable-line no-unused-vars
  
  // Setting the username from args
  let username = args

  // Our File Cache for the user - DISCORDID
  const path = 'cache/' + message.author.id + ".user"
  
  
  // Setting up our Request, using getUserByName method 
  var Request = http.get('https://' + process.env.ROOT_DOMAIN + '/jsonrpc.php').headers({ Accept: 'application/json', 'Content-Type': 'application/json' }).send({ "jsonrpc": "2.0", "method": "getUserByName", "id": 0, "params": { "username": username[0] } });

  // Begin the request and send authenication using the jsonrpc2.0 protocol.
  Request.auth({
    user: 'jsonrpc',
    pass: process.env.KANBOARD_API_KEY,
    sendImmediately: false
  }).then(function (response) {
    
    // We have a response, lets set it as a var
    let data = response.body.result

    // using fs to check for the cache, if it exists, lets tell the user
    if (fs.existsSync(path)) {
      console.log("User already exists")
      message.channel.send("That user is already in our system and cannot be registered again.")
    }
    else {
      // Ok, there is no file, lets make one!
      fs.writeFile('./cache/' + message.author.id + ".user", "{\"userid\":\"" + data.id + "\"}", function (err) {
       message.channel.send("You have registered your DiscordID successfully!")
      })
    }
  })

};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};

exports.help = {
  name: "register",
  category: "Main",
  description: "Bind your discordID to your board username.",
  usage: "register $BOARDUSERNAME"
};
