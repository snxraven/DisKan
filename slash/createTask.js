// For HTTP Requests
var http = require('unirest');
let count = 0

const jsonfile = require('jsonfile')

const { MessageActionRow, MessageSelectMenu } = require('discord.js');

require("dotenv").config();

exports.run = async (client, interaction) => {
  let interactionT = interaction;
  let boardList = []
  let embed = []
  let pages = []
  let projectID
  let title
  let userID
  let inter = interaction
  var rand = Math.floor(Math.random() * 999999999);
  // eslint-disable-line no-unused-vars
  await interaction.deferReply();
  console.log("DeferReply called")

  const jsonfile = require('jsonfile')
  const usercache = 'cache/' + interaction.user.id + '.user'
  jsonfile.readFile(usercache)
    .then(obj => {

      userID = obj.userid


      // Setting up our Request, using getAllProjects method 
      var Request = http.get('https://' + process.env.ROOT_DOMAIN + '/jsonrpc.php').headers({ Accept: 'application/json', 'Content-Type': 'application/json' }).send({ "jsonrpc": "2.0", "method": "getAllProjects", "id": 0 });

      // Begin the request and send authenication using the jsonrpc2.0 protocol.
      Request.auth({
        user: 'jsonrpc',
        pass: process.env.KANBOARD_API_KEY,
        sendImmediately: false
      }).then(function (response) {
        // console.log(response)
        // We have a response, lets set up a var
        let data = response.body.result

        // Setting up the correct formatting for our paginator 
        const pusherFunc = board => boardList.push({ label: board.name, description: board.name, value: board.id });
        data.forEach(pusherFunc);

        console.log(boardList)
        const row = new MessageActionRow()
          .addComponents(
            new MessageSelectMenu()
              .setCustomId('select')
              .setPlaceholder('Select a Project')
              .addOptions(boardList),
          );

        (async function () {
          await interaction.editReply({ content: 'Use the menu below to choose a Project', components: [row] });
        })()

        // await interaction.reply({ content: 'Pong!', components: [row] });
        client.on('interactionCreate', interaction => {
          if (!interaction.isSelectMenu()) return;
          console.log(interaction)
        })

        client.on('modalSubmit', (modal) => {

          if (interaction.customId === 'select') {
            console.log(interaction.values[0]);
            projectID = interaction.values[0]

            let titleCompontent = new TextInputComponent() // We create an Text Input Component
              .setCustomId('title') // We set the customId to title
              .setLabel('Title Name Here')
              .setStyle('SHORT') //IMPORTANT: Text Input Component Style can be 'SHORT' or 'LONG'
              .setMinLength(4)
              .setMaxLength(15)
              .setPlaceholder('Write a text here')
              .setRequired(true) // If it's required or not
              .setValue('value')

            let descCompontent = new TextInputComponent() // We create an Text Input Component
              .setCustomId('desc') // We set the customId to title
              .setLabel('Description')
              .setStyle('LONG') //IMPORTANT: Text Input Component Style can be 'SHORT' or 'LONG'
              .setMinLength(4)
              .setMaxLength(250)
              .setPlaceholder('Write a text here')
              .setRequired(true) // If it's required or not
              .setValue('value')

            let components = [titleCompontent, descCompontent]
            console.log(components)

            const modal = new Modal() // We create a Modal
              .setCustomId('title')
              .setTitle('Task Information')
              .addComponents(components);




            if (modal.customId === 'title') {
              showModal(modal, {
                client: client, // The showModal() method needs the client to send the modal through the API.
                interaction: interaction // The showModal() method needs the interaction to send the modal with the Interaction ID & Token.
              })


            }
          }

          (async () => {

            title = modal.getTextInputValue('title')
            desc = modal.getTextInputValue('desc')
            var Request = http.post('https://' + process.env.ROOT_DOMAIN + '/jsonrpc.php').headers({ Accept: 'application/json', 'Content-Type': 'application/json' }).send({
              "jsonrpc": "2.0",
              "method": "createTask",
              "id": 1176509098,
              "params": {
                "owner_id": userID,
                "creator_id": userID,
                "date_due": "",
                "description": desc,
                "category_id": 0,
                "score": 0,
                "title": title,
                "project_id": projectID,
                "color_id": "yellow",
                "column_id": 0,
                "recurrence_status": 0,
                "recurrence_trigger": 0,
                "recurrence_factor": 0,
                "recurrence_timeframe": 0,
                "recurrence_basedate": 0
              }
            });

            // Begin the request and send authenication using the jsonrpc2.0 protocol.
            Request.auth({
              user: 'jsonrpc',
              pass: process.env.KANBOARD_API_KEY,
              sendImmediately: true
            }).then(function (response) {
              (async () => {

                console.log(response.body)


                await interaction.editReply({ content: 'Task Created!', components: [] });

              })()
              boardList = []
              embed = []
              pages = []
              projectID = null
              userID = null
              title = null
              desc = null
              count = 0
              titleCompontent = null
              descCompontent = null
              components = null
              modal = null
              rand = Math.floor(Math.random() * 999999999);

            }
            )
          })()


        })

      })

    })
}

exports.commandData = {
  name: "createtask",
  description: "generate a task",
  // options: [{ "name": "name", "description": "The name of your new task", "type": 3, "name": "projectname", "description": "The name of the project to create in", "type": 3 }],
  defaultPermission: true,
};

// Set guildOnly to true if you want it to be available on guilds only.
// Otherwise false is global.
exports.conf = {
  permLevel: "User",
  guildOnly: true
};