// Import our needed HTTP Lib
var http = require('unirest');
const { MessageActionRow, MessageSelectMenu } = require('discord.js');

// Our main list array - STORAGE
let boardList = []
let data
let desc
let columns = [];
let tasksInfo = []
let projectName
//Requiring our discord components and paginator
const { MessageEmbed, MessageButton } = require("discord.js");
const { Pagination } = require("pagination.djs");

// Setting up our Global Config
require("dotenv").config();

// Grab RUN - get Client, interaction from the bot
exports.run = async (client, interaction) => {

  // eslint-disable-line no-unused-vars
  // Defer to aloow for embed building
  await interaction.deferReply();

  // Setting up our Request, using getAllProjects method 
  var Request = http.get('https://' + process.env.ROOT_DOMAIN + '/jsonrpc.php').headers({ Accept: 'application/json', 'Content-Type': 'application/json' }).send({ "jsonrpc": "2.0", "method": "getAllProjects", "id": 0 });

  // Begin the request and send authenication using the jsonrpc2.0 protocol.
  Request.auth({
    user: 'jsonrpc',
    pass: process.env.KANBOARD_API_KEY,
    sendImmediately: false
  }).then(function (response) {

    // We have a response, lets set up a var
    let data = response.body.result

    // Setting up the correct formatting for our paginator 
    const pusherFunc = board => boardList.push({ label: board.name, description: board.name, value: board.id });
    data.forEach(pusherFunc);

    console.log(boardList)
    const row = new MessageActionRow()
      .addComponents(
        new MessageSelectMenu()
          .setCustomId('select')
          .setPlaceholder('Select a Project')
          .addOptions(boardList),
      );

    (async function () {
      await interaction.editReply({ content: 'Use the menu below to choose a Project', components: [row] });
    })()

    // await interaction.reply({ content: 'Pong!', components: [row] });
    client.on('interactionCreate', interaction => {
      if (!interaction.isSelectMenu()) return;

      if (interaction.customId === 'select') {
        (async () => {
          await interaction.update({ content: '󠀠', components: [] });
          console.log(interaction.values[0]);

          var Request = http.get('https://' + process.env.ROOT_DOMAIN + '/jsonrpc.php').headers({ Accept: 'application/json', 'Content-Type': 'application/json' }).send({ "jsonrpc": "2.0", "method": "getBoard", "id": 0, "params": [interaction.values[0]] });

          // Begin the request and send authenication using the jsonrpc2.0 protocol.
          Request.auth({
            user: 'jsonrpc',
            pass: process.env.KANBOARD_API_KEY,
            sendImmediately: false
          }).then(function (response) {
            data = response.body.result[0]
            console.log(data)

            const date = require('date-and-time');
            const now = new Date();

            if (data.description == '') {
              desc = "N/A"
            } else {
              desc = data.description
            }

            const pusherFunc = column => columns.push(column.title + " " + column.nb_tasks + " tasks.\n");
            data.columns.forEach(pusherFunc);

            let dataTasks = response.body.result[0].columns
            dataTasks.forEach(function (column) {
              if (column.tasks != []) {
                column.tasks.forEach(function (task) {
                  // console.log(task)
                  //console.log (task.title + " in " + task.column_name + " for " + task.project_name) 
                  if (task.is_active == 0) return
                  tasksInfo.push(task.title + " in " + task.column_name + "\n")

                })
              }
            });
            console.log(tasksInfo)

            const mainEmbed = new MessageEmbed()
                            // { name: 'link', value: "https://" + process.env.ROOT_DOMAIN + "/board/" + data.project_id, inline: true })

              .setColor('#0099ff')
              .addFields(
                { name: 'Project ID', value: data.project_id, inline: true },
                { name: 'Swimlane Description', value: desc },
                { name: 'Task Info', value: columns.join(" "), inline: true },
                { name: 'Open Task Breakdown', value: tasksInfo.join(" "), inline: false })
              .setTitle('Requested Details')
              .setDescription("Swimlane: " + data.name)
              .setFooter(date.format(now, 'MM/DD/YYYY hh:mm:ss'));

            (async () => {
              await interaction.editReply({ embeds: [mainEmbed] });
              // Clear the list
              boardList = [];
              columns = [];
              desc = "";
              tasksInfo = [];
            })();
          })
        })();

      }

      if (!interaction.isSelectMenu()) return;
      
      console.log(interaction)


      if (interaction.customId === 'select') {
        console.log(interaction.values[0]);
        projectID = interaction.values[0]



        const discordModals = require('discord-modals') // Define the discord-modals package!
        const { Modal, TextInputComponent, showModal } = require('discord-modals') // Now we extract the showModal method
        discordModals(client); // Provide the client to the discord-modals package


        let titleCompontent = new TextInputComponent() // We create an Text Input Component
          .setCustomId('title') // We set the customId to title
          .setLabel('Title Name Here')
          .setStyle('SHORT') //IMPORTANT: Text Input Component Style can be 'SHORT' or 'LONG'
          .setMinLength(4)
          .setMaxLength(15)
          .setPlaceholder('Write a text here')
          .setRequired(true) // If it's required or not
          .setValue('value')

        let descCompontent = new TextInputComponent() // We create an Text Input Component
          .setCustomId('desc') // We set the customId to title
          .setLabel('Description')
          .setStyle('LONG') //IMPORTANT: Text Input Component Style can be 'SHORT' or 'LONG'
          .setMinLength(4)
          .setMaxLength(250)
          .setPlaceholder('Write a text here')
          .setRequired(true) // If it's required or not
          .setValue('value')

        let components = [titleCompontent, descCompontent]
        console.log(components)

        const modal = new Modal() // We create a Modal
          .setCustomId('title')
          .setTitle('Task Information')
          .addComponents(components);

        showModal(modal, {
          client: client, // The showModal() method needs the client to send the modal through the API.
          interaction: interaction // The showModal() method needs the interaction to send the modal with the Interaction ID & Token.
        })

      }


      
    })

  })
}


exports.commandData = {
  name: "projectdetails",
  description: "Lists all of the current projects",
  options: [],
  defaultPermission: true,
};

// Set guildOnly to true if you want it to be available on guilds only.
// Otherwise false is global.
exports.conf = {
  permLevel: "User",
  guildOnly: true
};