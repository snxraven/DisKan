// Import our needed HTTP Lib
var http = require('unirest');

// Our main list array - STORAGE
let boardList=[]

//Requiring our discord components and paginator
const { MessageEmbed, MessageButton } = require("discord.js");
const { Pagination } = require("pagination.djs");

// Setting up our Global Config
require("dotenv").config();

// Grab RUN - get Client, interaction from the bot
exports.run = async (client, interaction) => {
  
  // Init our paginator, lets set up custom emojis, then add our limit from the global config - // These are NOT required
  const pagination = new Pagination(interaction, {
    firstEmoji: "⏮", // First button emoji
    prevEmoji: "◀️", // Previous button emoji
    nextEmoji: "▶️", // Next button emoji
    lastEmoji: "⏭", // Last button emoji
    limit: process.env.PER_PAGE, // number of entries per page
    idle: 30000, // idle time in ms before the pagination closes
    ephemeral: false, // ephemeral reply
    prevDescription: "",
    postDescription: "",
    buttonStyle: "SECONDARY",
    loop: false, // loop through the pages
  });
  
  // eslint-disable-line no-unused-vars
  // Defer to aloow for embed building
  await interaction.deferReply();
  
  // Setting up our Request, using getAllProjects method 
  var Request = http.get('https://' + process.env.ROOT_DOMAIN + '/jsonrpc.php').headers({ Accept: 'application/json', 'Content-Type': 'application/json' }).send({ "jsonrpc": "2.0", "method": "getAllProjects", "id": 0 });

    // Begin the request and send authenication using the jsonrpc2.0 protocol.
  Request.auth({
    user: 'jsonrpc',
    pass: process.env.KANBOARD_API_KEY,
    sendImmediately: false
  }).then(function (response) {

    // We have a response, lets set up a var
    let data = response.body.result

    // Setting up the correct formatting for our paginator 
    const pusherFunc = board=>boardList.push({name:"Project Name", value:board.name + "\nID: " + board.id + "\n" + "https://" + process.env.ROOT_DOMAIN + "/board/" + board.id});
    data.forEach(pusherFunc);
  
    // Getting the last of the embed set up
    pagination.setTitle("Project List");
    pagination.setDescription("This is a list of all currently active projects.");
    
    pagination.setColor("#00ff00");
    pagination.setFooter("Projects:");
    pagination.setTimestamp();
    
    pagination.addFields(boardList);
    pagination.paginateFields(true);
    
    // RENDER
    pagination.render();

    // Clear the list
    boardList = [];
    
  })
};

exports.commandData = {
  name: "list",
  description: "Lists all of the current projects",
  options: [],
  defaultPermission: true,
};

// Set guildOnly to true if you want it to be available on guilds only.
// Otherwise false is global.
exports.conf = {
  permLevel: "User",
  guildOnly: false
};