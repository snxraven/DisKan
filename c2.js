const { Permissions } = require("discord.js");
const discordModals = require('discord-modals') // Define the discord-modals package!
const { MessageActionRow, MessageSelectMenu } = require('discord.js');
var http = require('unirest');
var http = require('unirest');

exports.run = async (client, interaction) => { // eslint-disable-line no-unused-vars
  discordModals(client); // discord-modals needs your client in order to interact with modals+
  //await interaction.deferReply();
        const { Modal, TextInputComponent, showModal } = require('discord-modals') // Now we extract the showModal method

        let titleCompontent = new TextInputComponent() // We create an Text Input Component
          .setCustomId('title') // We set the customId to title
          .setLabel('Title Name Here')
          .setStyle('SHORT') //IMPORTANT: Text Input Component Style can be 'SHORT' or 'LONG'
          .setMinLength(4)
          .setMaxLength(15)
          .setPlaceholder('Write a text here')
          .setRequired(true) // If it's required or not
          .setValue('value')

        let descCompontent = new TextInputComponent() // We create an Text Input Component
          .setCustomId('desc') // We set the customId to title
          .setLabel('Description')
          .setStyle('LONG') //IMPORTANT: Text Input Component Style can be 'SHORT' or 'LONG'
          .setMinLength(4)
          .setMaxLength(250)
          .setPlaceholder('Write a text here')
          .setRequired(true) // If it's required or not
          .setValue('value')

        let components = [titleCompontent, descCompontent]
        console.log(components)

        const modal = new Modal() // We create a Modal
          .setCustomId('title')
          .setTitle('Task Information')
          .addComponents(components);
        // Let's say that the interaction will be an Slash Command called 'ping'.

        showModal(modal, {
          client: client, // The showModal() method needs the client to send the modal through the API.
          interaction: interaction // The showModal() method needs the interaction to send the modal with the Interaction ID & Token.
        })

};

exports.commandData = {
  name: "createtask2",
  description: "Test Command",
  options: [],
  defaultPermission: true,
};

// Set guildOnly to true if you want it to be available on guilds only.
// Otherwise false is global.
exports.conf = {
  permLevel: "User",
  guildOnly: true
};